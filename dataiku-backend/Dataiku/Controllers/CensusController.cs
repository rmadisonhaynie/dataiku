using System.Collections.Generic;
using System.Linq;
using Dataiku.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Dataiku.Controllers
{
    [Route("Census")]
    public class CensusController : Controller
    {

        private readonly CensusContext _censusContext;

        //Better put in a DB Config table that could be updated from admin pages
        private static readonly IList<Param> ParamList = new List<Param>
        {
            new Param {Label = "Age", QueryCode = 0, ColumnName = "Age"},
            new Param {Label = "Class of Worker", QueryCode = 1, ColumnName = "Class of Worker"},
            new Param {Label = "Industry Code", QueryCode = 2, ColumnName = "Industry Code"},
            new Param {Label = "Occupation Code", QueryCode = 3, ColumnName = "Occupation Code"},
            new Param {Label = "Education", QueryCode = 4, ColumnName = "Education"},
            new Param {Label = "Wage Per Hour", QueryCode = 5, ColumnName = "Wage Per Hour"},
            new Param {Label = "Last Education", QueryCode = 6, ColumnName = "Last Education"},
            new Param {Label = "Marital Status", QueryCode = 7, ColumnName = "Marital Status"},
            new Param {Label = "Major Industry Code", QueryCode = 8, ColumnName = "Major Industry Code"},
            new Param {Label = "Major Occupation Code", QueryCode = 9, ColumnName = "Major Occupation Code"},
            new Param {Label = "Mace", QueryCode = 10, ColumnName = "Mace"},
            new Param {Label = "Hispanic", QueryCode = 11, ColumnName = "Hispanice"},
            new Param {Label = "Sex", QueryCode = 12, ColumnName = "Sex"},
            new Param {Label = "Member of Labor", QueryCode = 13, ColumnName = "Member of Labor"},
            new Param {Label = "Reason for Unemployment", QueryCode = 14, ColumnName = "Reason for Unemployment"},
            new Param {Label = "Full Time", QueryCode = 15, ColumnName = "Fulltime"},
            new Param {Label = "Capital Gain", QueryCode = 16, ColumnName = "Capital Gain"},
            new Param {Label = "Capital Loss", QueryCode = 17, ColumnName = "Capital Loss"},
            new Param {Label = "Dividends", QueryCode = 18, ColumnName = "Dividends"},
            new Param {Label = "Income Tax Liability", QueryCode = 19, ColumnName = "Income Tax Liability"},
            new Param {Label = "Previous Residence Region", QueryCode = 20, ColumnName = "Previous Residence Region"},
            new Param {Label = "Previous Residence State", QueryCode = 21, ColumnName = "Previous Residence State"},
            new Param {Label = "Household with Family", QueryCode = 22, ColumnName = "Household-with-Family"},
            new Param {Label = "Household Simple", QueryCode = 23, ColumnName = "Household-Simple"},
            new Param {Label = "Weight", QueryCode = 24, ColumnName = "Weight"},
            new Param {Label = "MSA Change", QueryCode = 25, ColumnName = "Msa-Change"},
            new Param {Label = "Region Change", QueryCode = 26, ColumnName = "Reg-Change"},
            new Param {Label = "Within Region Change", QueryCode = 27, ColumnName = "Within-Reg-Change"},
            new Param {Label = "Lived Here", QueryCode = 28, ColumnName = "Lived-Here"},
            new Param {Label = "Migration Previous Residence in Sunbelt", QueryCode = 29, ColumnName = "Migration Prev Res in Sunbelt"},
            new Param {Label = "Number of Persons Worked for Employer", QueryCode = 30, ColumnName = "Num Persons Worked for Employer"},
            new Param {Label = "Family Members Under 118", QueryCode = 31, ColumnName = "Family Members Under 118"},
            new Param {Label = "Father Birth Country", QueryCode = 32, ColumnName = "Father Birth Country"},
            new Param {Label = "Mother Birth Country", QueryCode = 33, ColumnName = "Mother Birth Country"},
            new Param {Label = "Birth Country", QueryCode = 34, ColumnName = "Birth Country"},
            new Param {Label = "Citizenship", QueryCode = 35, ColumnName = "Citizenship"},
            new Param {Label = "Own Business or Self Employed", QueryCode = 36, ColumnName = "Own Business or Self Employed"},
            new Param {Label = "Fill Questionnaire for Veteran's admin", QueryCode = 37, ColumnName = "Fill Questionnaire for Veteran's admin"},
            new Param {Label = "Veterans Benefits", QueryCode = 38, ColumnName = "Veterans Benefits"},
            new Param {Label = "Weeks Worked in Year", QueryCode = 39, ColumnName = "Weeks Worked in Year"},
            new Param {Label = "Year", QueryCode = 40, ColumnName = "Year"},
            new Param {Label = "Salary Range", QueryCode = 41, ColumnName = "Salary Range"}
        };

        public CensusController(CensusContext censusContext)
        {
            _censusContext = censusContext;
        }

        // GET
        [Route("Aggregate")]
        public IActionResult GetAggregation([FromQuery] int queryCode)
        {
            var param = ParamList.FirstOrDefault(p => p.QueryCode == queryCode);

            if (param == null)
            {
                return BadRequest($"The queryCode '{queryCode}' is unknown");
            }

            var str = $"Select `{param.ColumnName}` as Value, count(*) as Count FROM census_learn_sql c where `{param.ColumnName}` is not null group by `{param.ColumnName}` order by Count desc";

            var groupings = _censusContext.Aggregations.FromSql(str).ToList();
            var totalCount = groupings.Count();

            return Ok(new {Values = groupings, TotalCount = totalCount});
        }

        [Route("Params")]
        public IActionResult GetParams()
        {
            return Ok(ParamList);
        }

        [Route("AvgAge")]
        public IActionResult GetAvgAge()
        {
            return Ok(_censusContext.Census.Average(c => c.Age).Value.ToString("F2"));
        }

        private class Param
        {
            public string Label { get; set; }

            public int QueryCode { get; set; }

            [JsonIgnore]
            public string ColumnName { get; set; }
        }
    }
}