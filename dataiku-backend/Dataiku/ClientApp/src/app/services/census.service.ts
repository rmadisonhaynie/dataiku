import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable()
export class CensusService {
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
  }

  public GetAvgAge() : Observable<number> {
    return this.http.get<number>(this.baseUrl + 'Census/AvgAge');
  }

  public GetParams() : Observable<Param[]> {
    return this.http.get<Param[]>(this.baseUrl + 'Census/Params');
  }

  public GetAggregations(queryCode: number) : Observable<Aggregations> {
    return this.http.get<Aggregations>(this.baseUrl + 'Census/Aggregate',
      {params: {'queryCode': queryCode.toString()}});
  }
}

export interface Aggregation {
  value: string;
  count: number;
}

export interface Aggregations {
  values: Aggregation[];
  totalCount: number;
}

export interface Param {
  label: string;
  queryCode: number;
}
