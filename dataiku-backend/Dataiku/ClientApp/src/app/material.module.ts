import { MatTabsModule, MatCardModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { NgModule } from "@angular/core";

@NgModule({
  imports: [MatTabsModule, MatCardModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule],
  exports: [MatTabsModule, MatCardModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule],
})
export class MaterialModule { }
