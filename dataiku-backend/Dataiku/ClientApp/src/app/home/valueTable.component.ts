import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {CensusService} from "../services/census.service";
import {MatPaginator, MatTableDataSource} from "@angular/material";

@Component({
  selector: 'valueTable',
  templateUrl: './valueTable.component.html',
})
export class ValueTableComponent implements OnInit, AfterViewInit {
  displayedColumns = ['value', 'count'];
  dataSource = new MatTableDataSource();
  totalCount: number;
  avgAge: number;
  isLoadingResults = true;
  @Input('queryCode') queryCode: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private censusService: CensusService) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    if (this.queryCode === 0) {
      this.censusService.GetAvgAge().subscribe(data => this.avgAge = data);
    }

    this.censusService.GetAggregations(this.queryCode).subscribe(data => {
      this.dataSource.data = data.values;
      this.totalCount = data.totalCount;
      this.isLoadingResults = false;
    });
  }
}
