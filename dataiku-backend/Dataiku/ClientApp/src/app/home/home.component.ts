import {Component, OnInit} from '@angular/core';
import {CensusService, Param} from "../services/census.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  params: Param[];
  selectedIndex: number = 0;

  constructor(private censusService: CensusService) {
  }

  ngOnInit() {
    this.censusService.GetParams().subscribe(data => this.params = data);
  }

  onTabClick(queryCode: number) {

  }
}
