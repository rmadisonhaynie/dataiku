using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dataiku.DataAccess
{
    [Table("census_learn_sql")]
    public class Census
    {
        [Key]
        [Column("Age")]
        [Description("Age")]
        public int? Age { get; set; }

        [Column("Class of Worker")]
        [Description("Class of Worker")]
        public string ClassOfWorker { get; set; }

        [Column("Industry Code")]
        [Description("Industry Code")]
        public string IndustryCode { get; set; }

        [Column("Occupation Code")]
        [Description("Occupation Code")]
        public string OccupationCode { get; set; }

        [Column("Education")]
        [Description("Education")]
        public string Education { get; set; }

        [Column("Wage Per Hour")]
        [Description("Wage Per Hour")]
        public string WagePerHour { get; set; }

        [Column("Last Education")]
        [Description("Last Education")]
        public string LastEducation { get; set; }

        [Column("Marital Status")]
        [Description("Marital Status")]
        public string MaritalStatus { get; set; }

        [Column("Major Industry Code")]
        [Description("Major Industry Code")]
        public string MajorIndustryCode { get; set; }

        [Column("Major Occupation Code")]
        [Description("Major Occupation Code")]
        public string MajorOccupationCode { get; set; }

        [Column("Mace")]
        [Description("Mace")]
        public string Mace { get; set; }

        [Column("Hispanice")]
        [Description("Hispanice")]
        public string Hispanice { get; set; }

        [Column("Sex")]
        [Description("Sex")]
        public string Sex { get; set; }

        [Column("Member of Labor")]
        [Description("Member of Labor")]
        public string MemberOfLabor { get; set; }

        [Column("Reason for Unemployment")]
        [Description("Reason for Unemployment")]
        public string ReasonForUnemployment { get; set; }

        [Column("Fulltime")]
        [Description("Fulltime")]
        public string Fulltime { get; set; }

        [Column("Capital Gain")]
        [Description("Capital Gain")]
        public string CapitalGain { get; set; }

        [Column("Capital Loss")]
        [Description("Capital Loss")]
        public string CapitalLoss { get; set; }

        [Column("Dividends")]
        [Description("Dividends")]
        public string Dividends { get; set; }

        [Column("Income Tax Liability")]
        [Description("Income Tax Liability")]
        public string IncomeTaxLiability { get; set; }

        [Column("Previous Residence Region")]
        [Description("Previous Residence Region")]
        public string PreviousResidenceRegion { get; set; }

        [Column("Previous Residence State")]
        [Description("Previous Residence State")]
        public string PreviousResidenceState { get; set; }

        [Column("Household-with-Family")]
        [Description("Household with Family")]
        public string HouseholdWithFamily { get; set; }

        [Column("Household-Simple")]
        [Description("Household Simple")]
        public string HouseholdSimple { get; set; }

        [Column("Weight")]
        [Description("Weight")]
        public string Weight { get; set; }

        [Column("Msa-Change")]
        [Description("MSA Change")]
        public string MsaChange { get; set; }

        [Column("Reg-Change")]
        [Description("Region Change")]
        public string RegChange { get; set; }

        [Column("Within-Reg-Change")]
        [Description("Within Region Change")]
        public string WithinRegChange { get; set; }

        [Column("Lived-Here")]
        [Description("Lived Here")]
        public string LivedHere { get; set; }

        [Column("Migration Prev Res in Sunbelt")]
        [Description("Migration Previous Residence in Sunbelt")]
        public string MigrationPrevResInSunbelt { get; set; }

        [Column("Num Persons Worked for Employer")]
        [Description("Number of Persons Worked for Employer")]
        public string NumPersonsWorkedForEmployer { get; set; }

        [Column("Family Members Under 118")]
        [Description("Family Members Under 118")]
        public string FamilyMembersUnder118 { get; set; }

        [Column("Father Birth Country")]
        [Description("Father Birth Country")]
        public string FatherBirthCountry { get; set; }

        [Column("Mother Birth Country")]
        [Description("Mother Birth Country")]
        public string MotherBirthCountry { get; set; }

        [Column("Birth Country")]
        [Description("Birth Country")]
        public string BirthCountry { get; set; }

        [Column("Citizenship")]
        [Description("Citizenship")]
        public string Citizenship { get; set; }

        [Column("Own Business or Self Employed")]
        [Description("Own Business or Self Employed")]
        public string OwnBusinessOrSelfEmployed { get; set; }

        [Column("Fill Questionnaire for Veteran's admin")]
        [Description("Fill Questionnaire for Veteran's admin")]
        public string FillQuestionnaireForVeteransAdmin { get; set; }

        [Column("Veterans Benefits")]
        [Description("Veterans Benefits")]
        public string VeteransBenefits { get; set; }

        [Column("Weeks Worked in Year")]
        [Description("Weeks Worked in Year")]
        public string WeeksWorkedInYear { get; set; }

        [Column("Year")]
        [Description("Year")]
        public string Year { get; set; }

        [Column("Salary Range")]
        [Description("Salary Range")]
        public string SalaryRange { get; set; }
    }
}