
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Dataiku.DataAccess
{
    public class CensusContext : DbContext
    {
        public CensusContext(DbContextOptions<CensusContext> options) : base(options) {}

        public DbSet<Census> Census { get; set; }
        public DbSet<Aggregation> Aggregations { get; set; }

    }


    public class Aggregation
    {
        [Key]
        public string Value { get; set; }

        public int Count { get; set; }

    }
}